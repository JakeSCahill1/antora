= How Component Versions are Sorted

On this page, you'll learn:

* [x] How Antora sorts components and component versions.
* [x] How Antora selects the latest version of a component.

Understanding how Antora sorts xref:component-version.adoc[component versions] is important when choosing your versioning scheme and its potential presentation and routing results in your site's UI.

[#sort-docs-components]
== Docs component sort order

Antora sorts component versions into xref:component-version.adoc#docs-component[documentation components] according to the xref:component-title.adoc[title key] defined in each component version's [.path]_antora.yml_ file.
If the `title` key isn't set, Antora falls back to the xref:component-name-key.adoc[name key] to sort the components.

Docs components are sorted in alphabetical order.
The resulting alphabetical list of components is used for display purposes in the xref:navigation:index.adoc#component-dropdown[component version selector menu].
Under each component title or name, a list of versions, sorted according to the <<version-sorting-rules,component version sorting rules>>, is presented in the menu.

[#version-schemes]
== Versioning schemes

Antora only considers the value of the `version` key when sorting the component versions of a docs component.
The xref:component-version-key.adoc[value assigned to the version key] determines what versioning scheme, and therefore what order, a component version is placed in under its component title or name in the xref:navigation:index.adoc#component-dropdown[component version selector menu].
The reserved values `~` and `null` define a xref:component-with-no-version.adoc[component version as unversioned].

[cols="1,4,1"]
|===
|Scheme |Valid Identifiers |Example

|Semantic
a|[%hardbreaks]
Integer
String that starts with a number and contains at least one dot (`.`) character
String that starts with `v`, followed by a number, and contains at least one dot (`.`) character
a|[%hardbreaks]
`30`
`2.4`
`v90.3`

|Named
|All values that don't match the semantic scheme or the reserved version values (`~`, `null`)
a|[%hardbreaks]
`edge`
`z4`

|Unversioned
|`~` +
`null`
|`~`
|===

[#version-sorting-rules]
== Component version sorting rules

Antora applies the following rules when sorting the component versions of a component:

. An unversioned component version is displayed in the component version selector menu before named versions if both are present.
. Named versions of a component are displayed in the component version selector menu before semantic versions if both are present.
. Named versions of a component are sorted in reverse alphabetical order (e.g., `z4`, `wish`, `lester`).
.. Version identifiers with uppercase letters aren't recommended in URLs and can prevent portability between web servers.
If you use uppercase letters, they come before lowercase letters (e.g., `A`, `a`).
. Semantic versions of a component are displayed in the component version selector menu after named versions if both are present.
. Semantic versions of a component are sorted in descending order
.. The leading `v` in a semantic identifier is ignored and versions are sorted according to the first number after the `v`.
.. The https://semver.org[semantic versioning ordering rules] are applied.

[#determine-version-order]
=== Determine the version order of a component

Let's assume that the component _RoseyDB_ contains several component versions.
Some versions use the semantic scheme: `v2.5`, `4.0`, and `3.9`.
Some are named: `utopia` and `vivid`.
According to Antora's sorting rules, the _RoseyDB_ component versions will be displayed in the reference UI in the order listed below.

.Sorted RoseyDB component versions
....
vivid
utopia
4.0
3.9
v2.5
....

Bear in mind that if the xref:component-display-version.adoc[display_version key] is defined in a component version's [.path]_antora.yml_ file, the value of `display_version` is shown instead of the `version` key's value.
Regardless of whether or not `display_version` is set, the component version is *always sorted according to the value assigned to its `version` key*.
Thus, to the reader's eyes, the versions may not appear to be sorted in the order described.

For instance, if the `utopia` version of _RoseyDB_ is assigned a `display_version` value of `3.0`, the _RoseyDB_ versions will still be sorted in the same order.
The `utopia` version is just labeled as `3.0` in the reference UI.

....
vivid
3.0
4.0
3.9
v2.5
....

Antora also determines the <<latest-version,latest component version>> of each component.
The latest version for _RoseyDB_ would be `vivid` because it's the first version in the sorted list and not defined as a prerelease.

[#latest-version]
== Latest component version

In addition to sorting the component versions, Antora identifies the most recent, stable component version of each component.
The [.term]*latest component version*, also called the [.term]*latest version*, is the first version in the sorted list that's not a xref:component-prerelease.adoc[prerelease].
If all versions are prereleases, then the first prerelease version in the list is selected.
An xref:component-with-no-version.adoc[unversioned component version] is always considered the latest version or latest prerelease version (if `prerelease` is set).

Antora uses the latest version of a component when qualifying an incoming resource ID reference from another component if the version coordinate wasn't specified or can't otherwise be determined.

TIP: The latest version is available as the property `latest` in the xref:antora-ui-default::templates.adoc#site[UI model].
